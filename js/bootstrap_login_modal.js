(function ($) {
  Drupal.behaviors.bootstrap_login_modal = {
      attach: function (context) {
        $("#login-modal", context).detach().appendTo("body");
        $("#register-modal", context).detach().appendTo("body");
      }
  };
})(jQuery);
